<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\FormController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [IndexController::class, 'dashboard']);
Route::get('/pendaftaran', [FormController::class, 'daftar']);
Route::post('/kirim', [FormController::class, 'kirim']);

//testing master template
Route::get('/master', function(){
    return view('layouts.master');
});

//Tugas
Route::get('/table', function(){
    return view('tugas.table');
});
Route::get('/data-tables', function(){
    return view('tugas.data-tables');
});