<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tugas Harian 1.2</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign up Form</h2>

    <form action="/kirim" method="post">
        @csrf
        <label>First name : </label><br><br>
        <input type="text" name="Fname"><br><br>
        <label>Last name : </label><br><br>
        <input type="text" name="Lname" id=""><br><br>
        <label>Gender :</label><br><br>
        <input type="radio" id="" name="Gender" value="1"> Male <br>
        <input type="radio" id="" name="Gender" value="2"> Female<br>
        <input type="radio" id="" name="Gender" value="3"> Other<br>
        <p>Nationality :</p> 
        <select name="nationality" id="">
            <option value="1">Indonesian</option>
            <option value="2">American</option>
            <option value="3">Singaporean</option>
            <option value="4">Malaysian</option>
        </select>
        <p>Language Spoken : </p>
        <input type="checkbox" name="Indonesia" id="indonesi" value="1" >
        <label for="indonesia">Bahasa Indonesia<br></label>
        <input type="checkbox" name="english" id="english" value="2" >
        <label for="english">English<br></label>
        <input type="checkbox" name="otherlang" id="otherlang" value="3" >
        <label for="otherlang">Other<br></label>
        <p>Bio :</p>
        <textarea name="bio" id="" cols="20", rows="8"></textarea>
        <br><br>
        <input type="submit" value="sign up">
    </form>
    

</body>
</html>