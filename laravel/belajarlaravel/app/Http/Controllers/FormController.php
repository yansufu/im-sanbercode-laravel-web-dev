<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function daftar(){
        return view('page.form');
    }

    public function kirim(Request $request){
        //dd($request);
        $namadepan = $request['Fname'];
        $namabelakang = $request['Lname'];

        return view('page.home', ['namaDepan'=> $namadepan, "namaBelakang" => $namabelakang]);
    }
}
