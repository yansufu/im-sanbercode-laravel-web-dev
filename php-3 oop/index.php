<?php

require_once('Animal.php');
require_once('frog.php');
require_once('ape.php');


$sheep = new Animal("shaun");

echo "Nama hewan : ". $sheep->name . "<br>"; // "shaun"
echo "Jumlah kaki : ". $sheep->legs. "<br>"; // 4
echo "Darah dingin? ". $sheep->cold_blooded . "<br><br>"; // "no"

$frog = new frog("kodok");

echo "Nama hewan : ". $frog->name . "<br>"; // "shaun"
echo "Jumlah kaki : ". $frog->legs. "<br>"; // 4
echo "Darah dingin? ". $frog->cold_blooded . "<br>"; // "no"
echo $frog->jump();
echo "<br>";

$ape = new ape("Kera Sakti");

echo "Nama hewan : ". $ape->name . "<br>"; // "shaun"
echo "Jumlah kaki : ". $ape->legs. "<br>"; // 4
echo "Darah dingin? ". $ape->cold_blooded . "<br>"; // "no"
echo $ape->yell();
echo "<br>";
?>